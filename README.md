# Animated B-Spline.

Given control points of B-spline. These points move within the screen animating the spline.

B-spline is transforming into composite Bezier curve with de Boor algorithm. Bezier curves are interpolated with de Casteljau algorithm.

Features:

* Adding and deletion of control points.
* Changing quality of interpolation by de Casteljau algorithm.
* Switching antialiasing.
* Changing speed of animation.
* Switching visible points and lines.

Screenshot of application window:

![Application window](https://bitbucket.org/Korchagin/animated-b-spline/raw/master/img/screenshot.png)
